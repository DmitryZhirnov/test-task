<?php

use App\Models\IDatabase;
use App\Models\MysqlDB;
use App\Src\App;

session_start();
require_once('core/bootstrap.php');

try {
    $container = new DI\Container();
    /**Устанавливаю класс БД используемый в моделях */
    $container->set(IDatabase::class, $container->get(MysqlDB::class));

    // Получаю роут запроса
    $route = (new App())->getRouter()->get(getenv('REQUEST_URI'), getenv('REQUEST_METHOD'));
    // Получаю класс контроллера
    $controller = $container->get($route['controller']);

    $container->call([$controller, $route['action']]);
} catch (\App\Src\Exceptions\DBConnectionException $ex) {
    echo render('500', ['error' => $ex->getMessage()]);
} catch (\Exception $ex) {
    echo render('500');
}
