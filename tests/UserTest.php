<?php

declare(strict_types=1);

namespace Tests;

use App\Models\Model;
use App\Models\MysqlDB;
use App\Models\User;
use PHPUnit\Framework\MockObject\MockObject;

final class UserTest extends TestCase
{
    /**
     * @var MockObject $db
     */
    private $db;
    /**
     * @var User $user
     */

    public function setUp(): void
    {
        $this->db = $this->createMock(MysqlDB::class);
        $this->user = new User($this->db);
    }

    public function testFillMethod(): void
    {

        $array = ['email' => 'mock@mock.org', 'full_name' => 'Mark'];

        PHPUnitUtil::callMethod($this->user, 'fill', [$array]);
        $this->assertEquals('mock@mock.org', $this->user->email);
        $this->assertEquals('Mark', $this->user->full_name);
    }

    public function testUserFindMethodReturnUserClass()
    {

        $this->db->method('selectRaw')
            ->willReturn(['full_name' => 'Mark', 'email' => 'mock@mock.org']);

        $this->assertTrue($this->user instanceof User);
    }

    public function testUserFillMethod()
    {
        $this->assertEquals(true, true);
    }
}
