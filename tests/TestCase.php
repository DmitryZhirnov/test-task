<?php

namespace Tests;

use App\Models\IDatabase;
use App\Models\MysqlDB;

class TestCase extends \PHPUnit\Framework\TestCase
{
    public function __construct($name= '', array $data = [], $dataName='')
    {
        parent::__construct($name, $data, $dataName);
        require_once('core/bootstrap.php');
    }
}
