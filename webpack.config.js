const path = require('path');

module.exports = {
    entry: ['@babel/polyfill', './assets/js/index.js'],
    mode: 'development',
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist'),
        chunkFilename: '[name].js',
        publicPath: '/dist/'
    },
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: [
                    {
						loader: 'file-loader',
						options: {
                            name: '[name].css'
						}
                    },
                    'extract-loader',
                    'css-loader?-url',
                    'sass-loader',
                ],
            },
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ["@babel/preset-env"]
                    }
                }
            }
        ],
    }
};