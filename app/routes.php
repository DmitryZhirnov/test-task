<?php

use App\Controllers\LoginController;
use App\Controllers\AppController;
use App\Controllers\RegistrationController;

return [
    [
        'method' => 'GET',
        'controller' => AppController::class,
        'action' => 'index',
        'path' => '/'
    ],
    [
        'method' => 'POST',
        'controller' => AppController::class,
        'action' => 'lang',
        'path' => '/lang'
    ],
    [
        'method' => 'GET',
        'controller' => LoginController::class,
        'action' => 'index',
        'path' => '/login'
    ],
    [
        'method' => 'POST',
        'controller' => LoginController::class,
        'action' => 'login',
        'path' => '/login'
    ],
    [
        'method' => 'GET',
        'controller' => RegistrationController::class,
        'action' => 'index',
        'path' => '/registration'
    ],
    [
        'method' => 'POST',
        'controller' => RegistrationController::class,
        'action' => 'registration',
        'path' => '/registration'
    ],
    [
        'method' => 'GET',
        'controller' => LoginController::class,
        'action' => 'logout',
        'path' => '/logout'
    ],
    [
        
        'method' => 'POST',
        'controller' => RegistrationController::class,
        'action' => 'find',
        'path' => '/check-email'
    ]
];
