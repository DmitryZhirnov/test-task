<?php

namespace App\Src\Exceptions;

class InvalidCredentialsException extends \Exception
{
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
