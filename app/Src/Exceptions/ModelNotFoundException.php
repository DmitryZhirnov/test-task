<?php

namespace App\Src\Exceptions;

class ModelNotFoundException
{
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
