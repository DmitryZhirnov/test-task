<?php

namespace App\Src\Exceptions;

class DBConnectionException extends \Exception
{
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}
