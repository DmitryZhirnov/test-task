<?php
namespace App\Src\Exceptions;

class RequestValidationException extends \Exception
{
    public function __construct($errors)
    {
        $this->message = $errors;
    }
}
