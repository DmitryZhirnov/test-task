<?php

namespace App\Src\Services;

class ImageUploader
{
    /**
     * Загрузка файла профиля на сервер
     * @param $file - файл профиля
     * @return string - путь к файлу профиля
     */
    public static function uploadImage($file, $storagePath): string
    {
        $fileName = basename($file['name']);
        $ext = pathinfo($fileName, PATHINFO_EXTENSION);
        $uploadfile = time() . '.' . $ext;
        move_uploaded_file($file['tmp_name'], "{$storagePath}/{$uploadfile}");
        return $uploadfile;
    }
}
