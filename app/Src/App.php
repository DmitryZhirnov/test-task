<?php

namespace App\Src;

class App
{
    private $router;
    public function getRouter()
    {
        if (!isset($this->router)) {
            $routes = require_once('app/routes.php');
            $this->router = new Router($routes);
        }
        return $this->router;
    }
}
