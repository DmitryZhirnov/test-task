<?php

namespace App\Src;

use App\Src\Exceptions\PageNotFoundException;

class Router
{
    // Роуты приложения
    private $routes;

    public function __construct($routes)
    {
        $this->routes = $routes;
    }

    /**
     * Получить роут по пути и методу
     * @param $path - путь
     * @param $method - метод (POST, GET и т.д.)
     * @return array $route
     */
    public function get(string $path, string $method): array
    {
        foreach ($this->routes as $route) {
            $matchPath = str_replace('/', '\/', $route['path']);
            $match = preg_match('/^' . $matchPath . '\/?(\?.+)?$/', $path, $matches);

            if ($match && $route['method'] == $method) {
                return $route;
            }
        }
        page_not_found();
    }
}
