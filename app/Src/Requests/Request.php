<?php

namespace App\Src\Requests;

class Request
{
    protected $errors = [];
    protected $fields = [];
    public $files = [];
    
    public function __construct()
    {
        $this->files = $_FILES;

        $this->fields = $_GET + $_POST;
        
        foreach ($this->fields as $key => $value) {
            //Для доступа в объектном стиле
            $this->$key = $value;
        }
    }

    /**
     * Функция валидации запроса
     * @return bool
     * @throws RequestValidationException если запрос не прошел валидацию
     */
    public function validate()
    {
    }

    /**
     * Получение списка ошибок валидации
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
    /**
     * Все поля запроса
     */
    public function all()
    {
        return $this->fields;
    }
}
