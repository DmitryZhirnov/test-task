<?php

namespace App\Src\Requests;

/**
 * Запрос регистрации пользователя в системе
 */
class UserRegistrationRequest extends Request
{
    public function validate()
    {
        $lang = get_lang();
        //Проверка валидности email
        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $this->errors['email'] = $lang['FAILED_EMAIL'];
        }
        //Проверка имени
        if ($this->full_name != filter_var($this->full_name, FILTER_SANITIZE_STRING)) {
            $this->errors['full_name'] = $lang['FAILED_FULL_NAME'];
        }
        //Пароли должны совпадать
        if ($this->password != $this->password2) {
            $this->errors['password2'] = $lang['PASSWORDS_NOT_EQUALS'];
        }

        if (empty($this->errors)) {
            return true;
        }
            
        return false;
    }
}
