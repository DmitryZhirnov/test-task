<?php

namespace App\Controllers;

use App\Models\User;
use App\Src\Requests\Request;
use App\Src\Requests\UserRegistrationRequest;

class RegistrationController
{
    public function index()
    {
        echo render('registration');
    }

    public function registration(UserRegistrationRequest $request, User $user)
    {
        $_SESSION['registration'] = $request->all();

        if ($request->validate()) {

            // Регистрируем нового пользователя
            $user->create($request->all(), $request->files['avatar']);

            redirect('/');
        }
        echo render('registration', ['errors' => $request->getErrors()]);
    }

    /** Поиск пользователя по email */
    public function find(Request $request, User $user)
    {
        if (isset($request->email)) {
            echo json_encode($user->find($request->email));
            return;
        }
        echo json_encode('{}');
    }
}
