<?php

namespace App\Controllers;

use App\Models\User;
use App\Src\Requests\Request;
use App\Src\Exceptions\InvalidCredentialsException;

class LoginController
{
    public function index()
    {
        echo render('login');
    }

    public function login(Request $request, User $user)
    {
        try {
            $user->login($request->email, $request->password);
            redirect('/');
        } catch (InvalidCredentialsException $ex) {
            echo render('login', ['errors' => ['login' => $ex->getMessage()]]);
        } catch (\Exception $ex) {
            /** Ошибка на сервере
             * Пишем лог
             * Показываем страницу 500.php
             */
        }
    }

    public function logout()
    {
        unset($_SESSION['user']);
        redirect('/login');
    }
}
