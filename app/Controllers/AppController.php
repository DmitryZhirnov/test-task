<?php

namespace App\Controllers;

use App\Src\Requests\Request;

class AppController
{
    public function index()
    {
        if (!isset($_SESSION['user'])) {
            redirect('/login');
        }

        echo render('profile', $_SESSION['user']);
    }

    public function lang(Request $request)
    {
        /** Меняю язык */
        $_SESSION['lang'] = $request->lang;
        /** Возвращаемся откуда пришли */
        redirect(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_PATH));
    }
}
