<?php

namespace App\Models;

class Model
{
    /**
     * @var int $id;
     */
    private $id;
    /**
     * @var IDatabase $db
     */
    protected $db;
    /**
     * @var string $table
     */
    protected $table;
    /**
     * @var array $fillable
     */
    protected $fillable = [];
    /**
     * @var array $hide
     */
    protected $hide = [];

    public function __construct(IDatabase $db)
    {
        $this->db = $db;
        $this->id = 0;
    }

    /**
     * Заполнение объекта из массива
     */
    protected function fill(array $raw)
    {
        foreach ($this->fillable as $fieldName) {
            if (isset($raw[$fieldName]) && !in_array($fieldName, $this->hide)) {
                $this->$fieldName = $raw[$fieldName];
            }
        }
    }

    /**
     * Сериализация объекта в массив
     */
    public function toArray(): array
    {
        $raw = [];
        foreach ($this->fillable as $fieldName) {
            if (!array_key_exists($fieldName, $this->hide)) {
                $raw[$fieldName] = $this->$fieldName;
            }
        }
        return $raw;
    }
}
