<?php

namespace App\Models;

use App\Src\Exceptions\DBConnectionException;
use mysqli;
use mysqli_stmt;

/**
 * Класс подключения к БД
 */
class MysqlDB implements IDatabase
{
    public $connection;
    private $settings = [];
    /**
     * Получаю настройки БД и подключаюсь к ней
     */
    public function __construct()
    {
        $this->settings = include base_path('config/database.php');

        $this->connection = new mysqli(
            $this->settings['host'],
            $this->settings['user'],
            $this->settings['password'],
            $this->settings['db']
        );

        if ($this->connection->connect_error) {
            throw new DBConnectionException(get_lang()['DB_CONNECTION_ERROR']);
        }
    }
    /**
     * Закрываю соединение к БД
     */
    public function __destruct()
    {
        if (!$this->connection->connect_error) {
            $this->connection->close();
        }
    }

    public function insert(string $table, array $fields, array $fillable, string $paramTypes = '')
    {
        // Список значений для вставки в БД
        $values = array_map(function ($field) use ($fields) {
            return $fields[$field];
        }, $fillable);
        // Плейсхолдеры
        $prepValues = implode(',', array_fill(0, count($fillable), '?'));
        // Список полей
        $tableFields = implode(',', $fillable);
        // Формирую запрос
        $sql = "INSERT INTO {$table} ({$tableFields}) VALUES ({$prepValues})";
        $this->executePreparedSql($sql, $paramTypes, $values);
    }

    public function selectRaw(string $sql, array $values, array $fillable, string $paramTypes)
    {
        $builder = $this->executePreparedSql($sql, $paramTypes, $values);
        $result = $builder->get_result();
        if ($result->num_rows > 0) {
            return $result->fetch_assoc();
        }
        return null;
    }

    /**
     * Подготовка и выполнение запроса
     * @param $sql - тект запроса с плейсхолдерами
     * @param $paramTypes - плейсхолдеры
     * @param $values - значения подставляемые в плейсхолдеры
     * @return mysqli_stmt
     */
    private function executePreparedSql(string $sql, string $paramTypes, array $values): mysqli_stmt
    {
        // Подготовка запроса
        $builder = $this->connection->prepare($sql);
        $builder->bind_param($paramTypes, ...$values);
        // Запись в БД
        $builder->execute();
        return $builder;
    }
}
