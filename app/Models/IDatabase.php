<?php

namespace App\Models;

interface IDatabase
{
    /**
     * Вставка записи в базу данных
     * @param string $table - таблица БД
     * @param array $fields - ассоциативный массив значений для вставки в БД
     * @param array $fillable - список полей необходимый для вставки записи в БД
     * @param string $paramTypes - Типы плейскхолдеров вставляемых значений
     */
    public function insert(string $table, array $fields, array $fillable, string $paramTypes = '');
    /**
     * Выборка сырого запроса
     * @param string $query - сырой запрос
     * @param array $values - значения плейсхолдеров
     * @param array $fillable - список полей для отображения
     * @param string $paramTypes - Типы плейскхолдеров вставляемых значений
     */
    public function selectRaw(string $sql, array $values, array $fillable, string $paramTypes);
}
