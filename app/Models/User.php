<?php

namespace App\Models;

use App\Src\Exceptions\InvalidCredentialsException;
use App\Src\Exceptions\ModelNotFoundException;
use App\Src\Requests\Request;
use App\Src\Services\ImageUploader;

class User extends Model
{
    protected $table = 'users';
    protected $fillable = ['full_name', 'email', 'avatar', 'password'];
    protected $hide = ['password'];


    public function create(array $fields, $file)
    {
        //Загружаю изображение профиля
        $avatar = '';
        if (!empty($file['name'])) {
            $avatar = ImageUploader::uploadImage($file, profile_path());
        } else {
            $avatar = $_ENV['DEFAULT_PROFILE_IMAGE'];
        }
        $fields['avatar'] = $_ENV['PROFILES_DIR'] . $avatar;
        $fields['password'] = md5($fields['password']);

        $this->db->insert($this->table, $fields, $this->fillable, 'ssss');
    }

    /**
     * Вход
     * @param string $email
     * @param string $password
     */
    public function login($email, $password)
    {
        $sql = "SELECT * FROM users WHERE email=? AND password=?";
        $password =  md5($password);
        $raw = $this->db->selectRaw($sql, [$email, $password], $this->fillable, 'ss');

        if ($raw == null) {
            throw new InvalidCredentialsException(get_lang()['INVALID_CREDENTIALS']);
        }

        unset($_SESSION['user']);
        $_SESSION['user'] = $raw;
    }
    /**
     * Поиск пользователя по email
     */
    public function find(string $email): User
    {
        $sql = "SELECT * FROM users WHERE email=?";
        $raw = $this->db->selectRaw($sql, [$email], $this->fillable, 's');
        if ($raw === null) {
            throw new ModelNotFoundException('User not found');
        }
        $user = new User($this->db);
        $user->fill($raw);
        return $user;
    }


}
