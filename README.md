# Тестовое задание

---

Для того чтобы развернуть приложение на локальной машине необходимо выполнить следующие действия:

- Клонируйте репозиторий `git clone https://gitlab.com/DmitryZhirnov/test-task`
- Скопируйте файл .env.example в .env `cp .env.example .env`
- Запустить приложение `docker-compose up -d`, которое будет доступно по адресу http://localhost:8081


В приложении используются composer для работы с классами (autoload)  несколько зависимостей:
+   __php-di/php-di__ __Ioc__ контейнер для внедрения зависимостей
+   __symfony/dotenv__ - для переменных окружения

Для сборки js используется __webpack + babel__. Для работы с css используется __sass__.

На backend все запросы обрабатываются в файле index.php, который с помощью __app/Src/Router.php__ и __app/routes.php__
обрабатывает запросы и направляет их на соответствующие контроллеры. Контроллер обрабатывает запрос, получает данные
с помощью модели. Далее на основе полученных данных, рендерится отображение, которое отправляется пользователю.

На странице регистрации проверяется уникальность email, асинхронно проверяется наличие в БД.  

Поддержка языков реализована и на backend и на front