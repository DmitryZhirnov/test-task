<?php

use Symfony\Component\Dotenv\Dotenv;

/** Загрузка классов с помощью Composer */
require __DIR__ . '/../vendor/autoload.php';

/** Загрузка переменных окружения */
$dotenv = new Dotenv();
$dotenv->load(__DIR__ . '/../.env');

/** Загрузка хелперов */
require_once('helpers.php');

$APP_SETTINGS = include base_path('config/app.php');

require_once('language.php');
