<?php

/** Загрузка языков */
$languages_path = base_path('config/lang');
$language_files = scandir($languages_path);

if (!isset($_SESSION['lang'])) {
    $_SESSION['lang'] = $APP_SETTINGS['lang'];
}

foreach ($language_files as $file) {
    $file = $languages_path . '/' . $file;

    if (is_file($file)) {
        $fileName = pathinfo($file, PATHINFO_FILENAME);
        $_SESSION[$fileName] = include_once $file;
    }
}
