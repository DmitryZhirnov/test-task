<?php

/**
 * Функции хелперы
 */
/**
 * Путь от корня приложения до $path
 * @param $path - путь к файлу или директории
 */
function base_path(string $path = '')
{
    return __DIR__ . "/../{$path}";
}

/**
 * Формирование пути к необходимому файлу View на основании его имени файла
 * @param $view - имя файла отображения
 */
function view_path(string $view)
{
    return base_path() . "views/{$view}.php";
}
/**
 * Формирование пути к файлам профиля
 * @param $path - путь к файлу или директории
 */
function profile_path($path = '')
{
    return base_path("storage/images/profiles/{$path}");
}

/**
 * Функция рендеринга отображений
 */
function render(string $view, array $data = [])
{
    ob_start();
    extract($data, EXTR_SKIP);
    require view_path($view);
    return trim(ob_get_clean());
}

/**
 * Редирект
 */
function redirect(string $path)
{
    header('HTTP/1.1 200 OK');
    header("Location: http://{$_SERVER['HTTP_HOST']}{$path}");
    exit();
}

/**
 * Вывод ошибки 404
 */
function page_not_found()
{
    http_response_code(404);
    $content = view_path('404');
    require view_path('layout');
    exit();
}
/**
 * Получение списка фраз для текущего языка
 */
function get_lang(): array
{
    return $_SESSION[$_SESSION['lang']];
}

function is_login()
{
    return isset($_SESSION['user']);
}
