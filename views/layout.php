<!DOCTYPE html>
<html lang="<?= $_SESSION['lang'] ?>">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/dist/index.css">
    <script src="/dist/main.js"></script>
    <title><?= $title ?></title>
</head>

<body>
    <header>

        <form action="/lang" method="POST" class="lang_form">
            <input type="submit" name="lang" value="en">
            <input type="submit" name="lang" value="ru">
        </form>
        
        <?php
        if (is_login()===true) {?>
        <a href="/logout"><?= get_lang()['LOGOUT'] ?></a>
        
        <?php }?>
    </header>
    <main>
        <?php if (isset($content)) {
            echo $content;
        } ?>
    </main>
</body>

</html>