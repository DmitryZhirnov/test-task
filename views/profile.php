<?php
ob_start();
?>

<div class="profile">
    <div class="profile_cart">
        <div class="profile_image">
            <img src="<?= $avatar ?>">
        </div>
        <div class="profile_fullname"><?=get_lang()['FULL_NAME']?>: <?= $full_name ?></div>
        <div class="profile_email">Email: <?= $email ?></div>
    </div>
</div>
<?php
// Буфер в переменную
$content = ob_get_clean();
// Подключаю шаблон
include "layout.php";
