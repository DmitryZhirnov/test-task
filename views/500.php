<?php
ob_start();
?>
<h1>
    <?php if (isset($error)) { ?>
        <?= $error ?>
    <?php } else { ?>

        <?= get_lang()['500'] ?>
    <?php } ?>
</h1>
<?php
$content = ob_get_clean();
include "layout.php";
