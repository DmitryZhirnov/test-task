<?php
// Получаю текущий язык
$lang = get_lang();
// Вывод в буфер
ob_start();
?>
<form action="/login" method="POST" class="login-form">
    <label for="email">E-mail</label>
    <input type="email" placeholder="<?= $lang['ENTER_EMAIL'] ?>" name="email" required>
    <label for="password"><?= $lang['PASSWORD'] ?></label>
    <input type="password" placeholder="<?= $lang['ENTER_PASSWORD'] ?>" name="password" required>
    <br/>
    <input type="submit" value="<?= $lang['LOGIN'] ?>">
    <div class="error"><?=$errors['login']?></div>

    <div class="auth-form__question"><?= $lang['HAVE_NO_LOGIN'] ?><a href="/registration"><?= $lang['SING_UP']?></a></div>
</form>
<?php
// Буфер в переменную
$content = ob_get_clean();
// Подключаю шаблон
include "layout.php";
