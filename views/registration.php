<?php


if ($_SESSION['registration']) {
    list($email, $full_name, $password, $password2) = array_values($_SESSION['registration']);
}
$lang = get_lang();

ob_start();
?>

<form action="/registration" method="POST" class="auth-form" enctype="multipart/form-data">
    <div class="email-field">
        <label for="email">E-mail</label>
        <input type="email" class="email-field__input" placeholder="<?= $lang['ENTER_EMAIL'] ?>" name="email" value="<?= $email ?>" required>
        <span></span>
    </div>
    <div class="validation-error" id="email"><?= $errors['email'] ?></div>

    <label for="full_name"><?= $lang['FULL_NAME'] ?></label>
    <input type="text" placeholder="<?= $lang['ENTER_FULL_NAME'] ?>" name="full_name" value="<?= $full_name ?>" required>
    <div class="validation-error"><?= $errors['full_name'] ?></div>

    <label for="avatar"><?= $lang['AVATAR'] ?></label>
    <input type="file" name="avatar">

    <label for="password"><?= $lang['PASSWORD'] ?></label>
    <input type="password" placeholder="<?= $lang['ENTER_PASSWORD'] ?>" name="password" value="<?= $password ?>" required>
    <div class="validation-error"><?= $errors['password'] ?></div>

    <label for="password2"><?= $lang['REPEAT_PASSWORD'] ?></label>
    <input type="password" placeholder="<?= $lang['REPEAT_PASSWORD'] ?>" name="password2" value="<?= $password2 ?>" required>
    <div class="validation-error"><?= $errors['password2'] ?></div>
    <br />
    <input type="submit" value="<?= $lang['SING_UP'] ?>" name="registration_button">

    <div><?= $lang['HAVE_LOGIN'] ?> <a href="/login"><?= $lang['LOGIN'] ?></a></div>
</form>

<?php

$content = ob_get_clean();
include "layout.php";

unset($_SESSION['registration']);
