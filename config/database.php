<?php
return [
    'host' => $_ENV['MYSQL_HOST'],
    'user' => $_ENV['MYSQL_USER'],
    'password' => $_ENV['MYSQL_PASS'],
    'db' => $_ENV['MYSQL_DATABASE']
];
