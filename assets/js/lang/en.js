export default {
    EMAIL_REQUIRED: "Email required",
    EMAIL_FAIL: "Invalid email format",
    EMAIL_BUSY: "Email is busy"
}