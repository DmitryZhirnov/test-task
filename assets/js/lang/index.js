/**
 * В шаблоне меняется язык у тега html
 * 
 */
let lang = document.querySelector('html').getAttribute('lang');

let messages = import(`./${lang}.js`)


export default messages;
