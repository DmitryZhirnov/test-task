export default {
    EMAIL_REQUIRED: "Email обязателен для заполнения",
    EMAIL_FAIL: "Неверный формат email",
    EMAIL_BUSY: "Email занят"
}