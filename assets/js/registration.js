
// Форма регистрации
let registrationForm = document.querySelector('form.auth-form')

let emailInput = registrationForm['email']

let emailVerify = true

import laguage from './lang'


emailInput?.addEventListener('change', async function (event) {

    let {default:{EMAIL_REQUIRED, EMAIL_FAIL, EMAIL_BUSY}} = await laguage

    // Поле email обязательно
    if(event.target.value === ""){
        setEmailFail(this, EMAIL_REQUIRED)
        return
    }
    /** Проверка корректности введенного email */
    if (!/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}/.test(event.target.value)) {
        setEmailFail(this, EMAIL_FAIL);
        return;
    }
    /**Проверка есть ли такой email в БД */
    let data = new FormData()
    data.append('email', event.target.value)

    let response = await fetch('/check-email', {
        method: 'POST',
        body: data
    })

    let json = await response.json()
    if (json.email === event.target.value)
        setEmailFail(this, EMAIL_BUSY)
    else
        setEmailChek(this)
})


registrationForm?.addEventListener('submit', function (e) {
    e.preventDefault();
    if (!emailVerify) {
        return;
    }
    this.submit();
})

/**
 * Установка класса css для input email не прошедшего валидацию
 * или если он занят
 * @param element 
 */
function setEmailFail(element, text) {

    document.getElementById('email').innerText = text
    element.classList.add('busy-email')
    element.classList.remove('free-email')
    emailVerify =false;

}
/**
 * Установка класса css для email прошедшего валидацию или свободного
 * @param element 
 */
function setEmailChek(element) {
    document.getElementById('email').innerText = ''
    element.classList.add('free-email')
    element.classList.remove('busy-email')
    emailVerify = true;
}